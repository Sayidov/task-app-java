package taskapp.demo.mapper;

import org.springframework.stereotype.Component;
import taskapp.demo.dto.TaskDto;
import taskapp.demo.entity.Task;
import taskapp.demo.repository.TaskRepository;

import java.util.Optional;

@Component
public class TaskMapper {

    private final TaskRepository taskRepository;

    public TaskMapper(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }


    public Task toEntity(TaskDto taskDto) {
        Task task = new Task();
        task.setDocDate(taskDto.getDocDate());
        task.setName(taskDto.getName());
        Optional<Task> byId = taskRepository.findById(taskDto.getParentDto().getId());
        byId.ifPresent(task::setParentId);
        return task;
    }

    public TaskDto toDto(Task task) {
        TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setDocDate(task.getDocDate());
        taskDto.setName(task.getName());
        if (task.getParentId() == null) {
            taskDto.setParentDto(null);
            return taskDto;
        } else {
            Optional<Task> parentTask = taskRepository.findById(task.getParentId().getId());
            if(parentTask.isPresent()) {
                taskDto.setParentDto(toDto(parentTask.get()));
            } else {
                taskDto.setParentDto(null);
                return taskDto;
            }
        }
        return taskDto;
    }
}
