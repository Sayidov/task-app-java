package taskapp.demo.service;

import org.springframework.stereotype.Service;
import taskapp.demo.dto.TaskDto;
import taskapp.demo.entity.Task;
import taskapp.demo.mapper.TaskMapper;
import taskapp.demo.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;

    public TaskService(TaskRepository taskRepository, TaskMapper taskMapper) {
        this.taskRepository = taskRepository;
        this.taskMapper = taskMapper;
    }

    public Task createTask(TaskDto taskDto) {
        return taskRepository.save(taskMapper.toEntity(taskDto));
    }

    public List<TaskDto> getTasks() {
        return taskRepository.findAll().stream().map(taskMapper::toDto).collect(Collectors.toList());
    }
}
