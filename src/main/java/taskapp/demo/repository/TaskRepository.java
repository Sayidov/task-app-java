package taskapp.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import taskapp.demo.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task,Long> {

}
