package taskapp.demo.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Data;
import taskapp.demo.entity.Task;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class TaskDto {
    private Long id;
    private String name;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate docDate;
    private TaskDto parentDto;
}
